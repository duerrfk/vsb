/**
 * Copyright 2019 Frank Duerr
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

pthread_t thread1, thread2;

pthread_mutex_t mutex;
pthread_cond_t ready1, ready2; 

volatile int number = 0;

void *thread1_run(void *args)
{
     while (true) {
	  int success = pthread_mutex_lock(&mutex);
	  if (success != 0) {
	       perror("Could not lock mutex");
	       return NULL; // exit thread
	  }

	  // Critical section: this thread tries to change a 1 to 0.

	  // Wait for number to become 1.
	  // While-loop protects from spurious wakeups
	  // (wakeup without someone calling signal).
	  while (number != 1) {
	       // Wait for other thread to set number to 1.
	       // The thread calling cnd_wait() must hold the lock.
	       // cnd_wait() will automatically release the lock.
	       success = pthread_cond_wait(&ready2, &mutex);
	       if (success != 0) {
		    perror("Could not wait");
		    return NULL; // exit thread
	       }
	       // This thread again holds the lock here.
	  }

	  // Number is now 1 -> set it to 0.
	  number = 0;
	  printf("Thread 1: number = %d \n", number);
	  
          // Signal Thread 2 that Thread 1 has set number to 0.
	  // It's OK to signal before or after the mutex is unlocked.
	  success = pthread_cond_signal(&ready1);
	  if (success != 0) {
	       perror("Could not signal");
	       return NULL; // exit thread
	  }
	       
	  pthread_mutex_unlock(&mutex);
     }
     
     return 0;
}

void *thread2_run(void *args)
{
     while (true) {
	  int success = pthread_mutex_lock(&mutex);
	  if (success != 0) {
	       perror("Could not lock mutex");
	       return NULL; // exit thread
	  }

	  // Critical section: this thread tries to change a 0 to 1.

	  // Wait for number to become 0.
	  // While-loop protects from spurious wakeups
	  // (wakeup without someone calling signal).
	  while (number != 0) {
	       // Wait for other thread to set number to 0.
	       // The thread calling cnd_wait() must hold the lock.
	       // cnd_wait() will automatically release the lock.
	       success = pthread_cond_wait(&ready1, &mutex);
	       if (success != 0) {
		    perror("Could not wait");
		    return NULL; // exit thread
	       }
	       // This thread again holds the lock here.
	  }

	  // Number is now 0 -> set it to 1.
	  number = 1;
	  printf("Thread 2: number = %d \n", number);
	  
          // Signal Thread 1 that Thread 2 has set number to 1.
	  // It's OK to signal before or after the mutex is unlocked.
	  success = pthread_cond_signal(&ready2);
	  if (success != 0) {
	       perror("Could not signal");
	       return NULL; // exit thread
	  }
	  
	  pthread_mutex_unlock(&mutex);
     }
     
     return 0;
}

int main(int argc, char *argv[])
{
     // Create mutex variable.
     int success = pthread_mutex_init(
	  &mutex, // pointer to mutex variable
	  NULL);  // (no) mutex attributes
     if (success != 0) {
	  perror("Could not create mutex variable");
	  return 1;
     }
     
     // Create conditional variables.
     success = pthread_cond_init(
	  &ready1, // pointer to conditional variable
	  NULL);   // (no) attributes
     if (success != 0) {
	  perror("Could not create conditional variable");
	  return 1;
     }
     success = pthread_cond_init(&ready2, NULL);
     if (success != 0) {
	  perror("Could not create conditional variable");
	  return 1;
     }
     
     // Start Thread 1.
     success = pthread_create(&thread1, NULL, thread1_run, NULL);
     if (success != 0) {
	  perror("Could not start Thread 1.");
	  return 1;
     }

     // Start Thread 2.
     success = pthread_create(&thread2, NULL, thread2_run, NULL);
     if (success != 0) {
	  perror("Could not start Thread 2.");
	  return 1;
     }
     
     success = pthread_join(thread1, NULL);
     if (success != 0) {
	  perror("Could not join Thread 1.");
	  return 1;
     }
     success = pthread_join(thread2, NULL);
     if (success != 0) {
	  perror("Could not join Thread 2.");
	  return 1;
     }
     
     return 0;
}
