#include <atomic>
#include "stdthreads_wrapper.h"

using namespace std;

volatile atomic_bool ready;
volatile int value;

thrd_t thread1, thread2;

int thread1_run(void* params)
{
	value = 42;
	atomic_store(&ready, true);

	return 0;
}

int thread2_run(void* params)
{
	while (!atomic_load(&ready));
	value += 43;

	printf("%d \n", value);

	return 0;
}

int main()
{
	int args1[] = { 1,2 };
	int success = thrd_create(&thread1, thread1_run, args1);
	if (success != thrd_success) {
		fprintf(stderr, "Could not start thread\n");
		return 1;
	}

	int args2[] = { 3,4 };
	success = thrd_create(&thread2, thread2_run, args2);
	if (success != thrd_success) {
		fprintf(stderr, "Could not start thread\n");
		return 1;
	}

	int res;
	success = thrd_join(thread1, &res);
	if (success != thrd_success) {
		fprintf(stderr, "Could not join thread 1\n");
		return 1;
	}

	success = thrd_join(thread2, &res);
	if (success != thrd_success) {
		fprintf(stderr, "Could not join thread 2\n");
		return 1;
	}

	return 0;
}