#include "stdthreads_wrapper.h"
#include <exception>

int thrd_create(thrd_t *thr, thrd_start_t func, void *args)
{
	try {
		*thr = std::make_shared<std::thread>(func, args);
	}
	catch (std::exception ex) {
		return thrd_error;
	}
	return thrd_success;
}

int thrd_join(thrd_t thr, int *res)
{
	try {
		thr->join();
	}
	catch (std::exception) {
		return thrd_error;
	}

	// Always return 0 as result since C++ threads do not return a result value.
	*res = 0;

	return thrd_success;
}
