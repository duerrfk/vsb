/**
 * Copyright 2019 Frank Duerr
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <pthread.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

pthread_t thread1, thread2;

volatile atomic_int mutex = 0;
volatile unsigned int counter = 0;

void lock_acquire(volatile atomic_int *mutex)
{
     // Busy waiting ("spinning") to enter critical section.
     while (atomic_exchange(mutex, 1) != 0);
     
     // Note: acquire semantic (implied by sequential consistency)
     // ensures that every memory access below (in critical section)
     // stays below (in critical section).
}

void lock_release(volatile atomic_int *mutex)
{
     // Note: release semantic (implied by sequential consistency)
     // ensures that every memory access above (in critical section)
     // stays above (in critical section).
     
     atomic_store(mutex, 0);
}

void *thread_run(void *args)
{
     int threadid = *((int *) (args));
     
     while (true) {
	  lock_acquire(&mutex);
	  // Critical Section
	  printf("Thread %d in critical section \n", threadid);
	  counter++;
	  if (counter > 1) {
	       fprintf(stderr, "Mutual exclusion violated\n");
	       exit(1); // exit process (terminates all threads)
	  }
	  counter--;
	  lock_release(&mutex);
     }
     
     return NULL;
}

int main(int argc, char *argv[])
{
     int thread1_id = 1;
     int success = pthread_create(&thread1, NULL, thread_run, &thread1_id);
     if (success != 0) {
	  perror("Could not start Thread 1");
	  exit(1);
     }

     int thread2_id = 2;
     success = pthread_create(&thread2, NULL, thread_run, &thread2_id);
     if (success != 0) {
	  perror("Could not start Thread 2");
	  exit(1);
     }

     success = pthread_join(thread1, NULL);
     if (success != 0) {
	  perror("Could not join Thread 1");
	  return 1;
     }
     success = pthread_join(thread2, NULL);
     if (success != 0) {
	  perror("Could not join Thread 2");
	  return 1;
     }

     return 0;
}
