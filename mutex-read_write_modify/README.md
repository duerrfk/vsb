Verschiedene Beispiel, wie atomare Read-Modify-Write-Befehle genutzt werden können, um kritische Abschnitte abzusichern:

* test & set
* compare & swap
* exchange
* fetch and add (ticket lock)
