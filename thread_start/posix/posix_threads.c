/**
 * Copyright 2019 Frank Duerr
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

pthread_t thread1, thread2;

// Threads will execute this function.
void *thread_run(void *args)
{
     int *numbers = (int *) args;
     int x = numbers[0];
     int y = numbers[1];
     int res = x+y;
     printf("Thread: %d + %d = %d\n", x, y, res);

     // Exiting thread function is equivalent to calling pthread_exit().
     return NULL;
}

int main(int argc, char *argv[])
{
     // Start Thread 1 and Thread 2 with two integer arguments.
     int args1[] = {1,2};
     int success = pthread_create(
	  &thread1,   // thread object
	  NULL,       // thread attributes, e.g., scheduling policies
	  thread_run, // thread function
	  args1       // arguments to thread function
	  );
     if (success != 0) {
	  perror("Could not start thread 1.");
	  exit(1);
     }

     // Start Thread 2.
     int args2[] = {42,43};
     success = pthread_create(&thread2, NULL, thread_run, args2);
     if (success != 0) {
	  perror("Could not start thread 2.");
	  exit(1);
     }

     // Main thread waits for Thread 1 and and Thread 2 to finish.
     success = pthread_join(
	  thread1, // the thread to join
	  NULL     // exit value (see pthread_exit())
	  ); 
     if (success != 0) {
	  perror("Join with thread 1 failed.");
	  exit(1);
     }
     success = pthread_join(thread2, NULL);
     if (success != 0) {
	  perror("Join with thread 2 failed.");
	  exit(1);
     }

     return 0;
}
