/**
 * Copyright 2019 Frank Duerr
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Threads
pthread_t thread1, thread2;

// Binary semaphore for mutual exclusion of critical section
sem_t semaphore;

void *thread_run(void *args)
{
     int thread_number = *((int *) args);
     
     while (true) {
	  // Wait for binary semaphore before entering critical section.
	  int success = sem_wait(&semaphore);
	  if (success != 0) {
	       perror("Could not wait for semaphore");
	       return 0; // exit thread
	  }
	  
	  printf("Thread %d in critical section. \n", thread_number);
	  
	  //  Signal semaphore after leaving critical section.
	  success = sem_post(&semaphore);
	  if (success != 0) {
	       perror("Could not signal semaphore");
	       return 0; // exit thread
	  }
     }
     
     return 0;
}

int main(int argc, char* argv[])
{
     // Create a binary semaphore.
     int success = sem_init(
	  &semaphore, // semaphore object
	  false,      // shared semaphore between processes (e.g., children)?
	  1           // initial semaphore count (one thread can pass)
	  );
     if (success != 0) {
	  perror("Could not create semaphore");
	  return 1;
     }
     
     // Create Thread 1.
     int arg1 = 1; // thread number as argument to thread function
     success = pthread_create(&thread1, NULL, thread_run, &arg1);
     if (success != 0) {
	  perror("Could not start Thread 1");
	  return 1;
     }

     // Create Thread 2.
     int arg2 = 2; // thread number as argument to thread function
     success = pthread_create(&thread2, NULL, thread_run, &arg2);
     if (success != 0) {
	  perror("Could not start Thread 2");
	  return 1;
     }

     // Wait for Thread 1.
     success = pthread_join(thread1, NULL);
     if (success != 0) {
	  perror("Could not join Thread 1");
	  return 1;
     }

     // Wait for Thread 2.
     success = pthread_join(thread2, NULL);
     if (success != 0) {
	  perror("Could not join Thread 2");
	  return 1;
     }

     return 0;
}
