#ifndef PRODUCER_CONSUMER
#define PRODUCER_CONSUMER

/**
 * Initialisiert den Producer/Consumer.
 *
 * @param buffer_size Gr��e des Job-Puffers.
 */
void producer_consumer_init(size_t buffer_size);

/**
 * Produzenten rufen diese Funktion auf, um Jobs in die Job-Warteschlange aufzunehmen.
 * Diese Funktion blockiert bis Platz f�r den neuen Job in der Warteschlange ist.
 * 
 * @param job Zeiger auf Job.
 */
void produce(void *job);

/**
 * Konsumenten rufen diese Funktion auf, um Jobs aus der Job-Warteschlange zu entnehmen.
 * Diese Funktion blockiert, bis ein Job vorhanden ist.
 *
 * @return Zeiger auf entnommenen Job.
 */
void *consume();

#endif

