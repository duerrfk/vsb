#include "ProducerConsumer.h"
#include "RingBuffer.h"
#include <Windows.h>
#include <stdio.h>
#include <cassert>

// Semaphore
HANDLE mutex;     // bin�res Semaphor um kritischen Abschnitt beim Zugriff auf den Puffer zu sperren
HANDLE not_empty; // Semaphor um verf�gbare Auftr�ge f�r Consumer zu z�hlen.
HANDLE not_full;  // Semaphor um freie Pufferpl�tze zu z�hlen.

// Ring buffer zur Verwaltung der Jobs.
struct ring_buffer buffer;

void producer_consumer_init(size_t buffer_size)
{
	ring_buffer_init(&buffer, buffer_size);

	// Semaphore erzeugen
	mutex = CreateSemaphore(
		NULL,         // Default Sicherheitsattribute
		1,            // initialer Semaphorz�hler (1 = Mutex nicht gesperrt)
		1,            // max. Semaphorz�hler (1 f�r bin�res Semaphor)
		NULL          // unbenanntes Semaphor
	);
	if (mutex == NULL) {
		fprintf(stderr, "Could not create semaphore (%d) \n", GetLastError());
		exit(1);
	}

	not_full = CreateSemaphore(
		NULL,         // Default Sicherheitsattribute
		buffer_size,  // initialer Semaphorz�hle: alle Pufferpl�tze frei
		buffer_size,  // max. Semaphorz�hler
		NULL          // unbenanntes Semaphor
	);
	if (not_full == NULL) {
		fprintf(stderr, "Could not create semaphore (%d) \n", GetLastError());
		exit(1);
	}

	not_empty = CreateSemaphore(
		NULL,         // Default Sicherheitsattribute
		0,            // initialer Semaphorz�hle: keine Auftr�ge im Puffer
		buffer_size,  // max. Semaphorz�hler
		NULL          // unbenanntes Semaphor
	);
	if (not_empty == NULL) {
		fprintf(stderr, "Could not create semaphore (%d) \n", GetLastError());
		exit(1);
	}
}

void produce(void* job)
{
	// Auf freien Pufferplatz warten.
	DWORD success = WaitForSingleObject(not_full, INFINITE);
	if (success == WAIT_FAILED) {
		fprintf(stderr, "Could not wait for semaphore (%d).\n", GetLastError());
		exit(1);
	}

	// Es ist jetzt ein Pufferplatz f�r diesen Producer frei.

	// Kritischen Abschnitt f�r Zugriff auf Puffer sperren, um Race-Condition zu vermeiden.
	// Beachte: Nebenl�ufiger Zugriff auf Puffer durch mehrere Consumer und Producer m�glich.
	// Puffer ist nicht thread-safe.
	success = WaitForSingleObject(mutex, INFINITE);
	if (success == WAIT_FAILED) {
		fprintf(stderr, "Could not wait for semaphore (%d).\n", GetLastError());
		exit(1);
	}

	// Kritischer Abschnitt: Anfang

	// Neuen Job in Puffer einf�gen. 
	// Puffer kann (darf) hier nicht voll sein.
	ring_buffer_add_head(&buffer, job);

	// Kritischer Abschnitt: Ende
	
	// Kritischen Abschnitt verlassen: bin�res Semaphor signalisieren
	BOOL bsuccess = ReleaseSemaphore(
		mutex,   // zu signalisierendes Semaphor
		1,       // Z�hler um 1 erh�hen
		NULL     // vorherigen Z�hler ermitteln (hier nicht notwendig)
	);
	if (!bsuccess) {
		fprintf(stderr, "Could not release semaphore (%d).\n", GetLastError());
		exit(1);
	}

	// Consumer signalisieren: Es ist ein neuer Auftrag im Puffer.
	bsuccess = ReleaseSemaphore(
		not_empty,   // zu signalisierendes Semaphor
		1,           // Z�hler um 1 erh�hen
		NULL         // vorherigen Z�hler ermitteln (hier nicht notwendig)
	);
	if (!bsuccess) {
		fprintf(stderr, "Could not release semaphore (%d).\n", GetLastError());
		exit(1);
	}
}

void* consume()
{
	// Auf Auftrag warten.
	DWORD success = WaitForSingleObject(not_empty, INFINITE);
	if (success == WAIT_FAILED) {
		fprintf(stderr, "Could not wait for semaphore (%d).\n", GetLastError());
		exit(1);
	}

	// Es ist jetzt ein Auftrag im Puffer f�r diesen Consumer.

	// Kritischen Abschnitt f�r Zugriff auf Puffer sperren, um Race-Condition zu vermeiden.
	// Beachte: Nebenl�ufiger Zugriff auf Puffer durch mehrere Consumer und Producer m�glich.
	// Puffer ist nicht thread-safe.
	success = WaitForSingleObject(mutex, INFINITE);
	if (success == WAIT_FAILED) {
		fprintf(stderr, "Could not wait for semaphore (%d).\n", GetLastError());
		exit(1);
	}

	// Kritischer Abschnitt: Anfang

	// Auftrag aus Puffer entnehmen. 
	// Puffer kann (darf) hier nicht leer sein.
	void* job = ring_buffer_remove_tail(&buffer);
	// job kann nicht NULL sein, denn der Puffer war garantiert nicht leer.
	assert(job != NULL);

	// Kritischer Abschnitt: Ende

	// Kritischen Abschnitt verlassen: bin�res Semaphor signalisieren
	BOOL bsuccess = ReleaseSemaphore(
		mutex,   // zu signalisierendes Semaphor
		1,       // Z�hler um 1 erh�hen
		NULL     // vorherigen Z�hler ermitteln (hier nicht notwendig)
	);
	if (!bsuccess) {
		fprintf(stderr, "Could not release semaphore (%d).\n", GetLastError());
		exit(1);
	}

	// Producer signalisieren: Es wurde ein Pufferplatz frei.
	bsuccess = ReleaseSemaphore(
		not_full,    // zu signalisierendes Semaphor
		1,           // Z�hler um 1 erh�hen
		NULL         // vorherigen Z�hler ermitteln (hier nicht notwendig)
	);
	if (!bsuccess) {
		fprintf(stderr, "Could not release semaphore (%d).\n", GetLastError());
		exit(1);
	}

	return job;
}