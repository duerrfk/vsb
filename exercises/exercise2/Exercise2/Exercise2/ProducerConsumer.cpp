#include "ProducerConsumer.h"
#include "RingBuffer.h"
#include <Windows.h>
#include <stdio.h>
#include <cassert>

// TODO
// AUfgabe 1: Semaphore deklarieren
// Aufgabe 2: MUTEX und Bediungungsvariablen deklarieren

// Ring buffer zur Verwaltung der Jobs.
struct ring_buffer buffer;

void producer_consumer_init(size_t buffer_size)
{
	ring_buffer_init(&buffer, buffer_size);

	// TODO
	// Aufgabe 1: Semaphore erzeugen
	// Aufgabe 2: MUTEX und Bedingungsvariablen erzeugen
}

void produce(void* job)
{
	// TODO

	// Neuen Job in Puffer einf�gen. 
	// Puffer kann (darf) hier nicht voll sein.
	ring_buffer_add_head(&buffer, job);

	// TODO
}

void* consume()
{
	// TODO

	// Auftrag aus Puffer entnehmen.
	// Puffer kann (darf) hier nicht leer sein.
	void* job = ring_buffer_remove_tail(&buffer);
	// job kann nicht NULL sein, denn der Puffer war garantiert nicht leer.
	assert(job != NULL);

	// TODO

	return job;
}