#include "ProducerConsumer.h"
#include "RingBuffer.h"
#include <Windows.h>
#include <cassert>

// Mutex um kritischen Abschnitt beim Zugriff auf Puffer zu sch�tzen.
CRITICAL_SECTION cs;
// Bedingungsvariablen um Pufferzustand (nicht voll, nicht leer) an Konsumenten bzw. Produzenten zu signalisieren.
CONDITION_VARIABLE not_empty, not_full;

// Ring buffer zur Verwaltung der Jobs.
struct ring_buffer buffer;

void producer_consumer_init(size_t buffer_size)
{
	ring_buffer_init(&buffer, buffer_size);

	// Create critical section (MUTEX) object.
	InitializeCriticalSection(&cs);

	// Create conditional variables.
	InitializeConditionVariable(&not_empty);
	InitializeConditionVariable(&not_full);
}

void produce(void* job)
{
	// Kritischen Abschnitt betreten (Mutex sperren).
	EnterCriticalSection(&cs);

	// Auf freie Pufferpl�tze warten.
	while (ring_buffer_count(&buffer) == ring_buffer_size(&buffer)) {
		// Puffer ist aktuell voll.
		// Auf Signal von Consumer warten, dass Pufferpl�tze frei sind.
		// Producer verl�sst w�hrend des Wartens den kritischen Abschnitt. 
		SleepConditionVariableCS(&not_full, &cs, INFINITE);
	}

	// Hier ist mindestens ein Pufferplatz frei. 
	// Der kritische Abschnitt ist wieder gesperrt, d.h. dieser Producer ist der
	// einzige Thread hier, der aktuell den Puffer modifiziert.

	// Neues Element in Puffer einf�gen. 
	// Puffer kann (darf) hier nicht voll sein.
	ring_buffer_add_head(&buffer, job);

	// Signal an wartende Consumer schicken: Es ist mindestens ein Element im Puffer.
	WakeConditionVariable(&not_empty);

	// Kritischen Abschnitt verlassen (Mutex entsperren).
	LeaveCriticalSection(&cs);
}

void* consume()
{
	// Kritischen Abschnitt betreten (Mutex sperren).
	EnterCriticalSection(&cs);

	// Darauf warten, dass mindestens ein Job im Puffer ist. 
	while (ring_buffer_count(&buffer) == 0) {
		// Puffer ist aktuell leer.
		// Auf Signal von Producer warten, dass neue Jobs in der Warteschlange sind.
		// Der Consumer verl�sst w�hrend des Wartens den kritischen Abschnitt.
		SleepConditionVariableCS(&not_empty, &cs, INFINITE);
	}

	// Hier ist mindestens ein Job in der Warteschlange.
	// Der kritische Abschnitt ist wieder gesperrt, d.h. dieser Thread ist der einzige,
	// der aktuell den Puffer modifiziert.

	// Auftrag aus Puffer entnehmen.
	// Puffer kann (darf) hier nicht leer sein.
	void* item = ring_buffer_remove_tail(&buffer);
	// item kann nicht NULL sein, denn der Puffer ist hier garantiert nicht leer.
	assert(item != NULL);

	// Signal an wartende Producer schicken: Es ist mindestens ein Pufferplatz frei.
	WakeConditionVariable(&not_full);

	// Kritischen Abschnitt verlassen (Mutex entsperren).
	LeaveCriticalSection(&cs);

	return item;
}