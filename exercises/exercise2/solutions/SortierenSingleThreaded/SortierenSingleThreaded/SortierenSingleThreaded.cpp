/**
 * Copyright 2019 Frank Duerr
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

 // Notwendig für rand_s()
#define _CRT_RAND_S

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>
// Da Visual Studio den stdatomic.h (C11-Standard) nicht unterstützt, verwenden wir die gleichnamigen C++ Typen und Funktionen aus atomic.h
#include <atomic>
#include <cassert>

// Da Visual Studio den C11-Standard (stdatomic.h) nicht unterstützt, verwenden wir die gleichnamigen C++ Typen und Funktionen.
// Hierzu ist die folgenden Zeile notwendig, um per Default den Standardnamensraum zu verwenden.
// Der C- und C++-Code ist ansonsten identisch!
using namespace std;

// Länge der zu sortierenden Arrays.
const size_t array_len = 10000000;
// Anzahl der zu sortierenden Arrays.
const size_t array_cnt = 10;

/**
 * Erzeugt ein Array mit zufälligen, nicht sortierten Daten.
 * Der Aufrufer ist verantwortlich, den Speicher des Arrays wieder mittels free() freizugeben.
 *
 * @param data Zeiger auf das erzeugte Array. NULL, im Fehlerfall.
 * @param len Länge des zu erzeugenden Arrays.
 */
void create_random_array(unsigned int** data, unsigned int len)
{
	*data = (unsigned int*)malloc(sizeof(unsigned int) * len);
	if (*data == NULL)
		return;

	for (size_t i = 0; i < len; i++)
		rand_s(&(*data)[i]);
}

/**
 * Quick-Sort: Partitionierung eine Sub-Arrays.
 * Nachbedingung: Pivot-Element steht an korrekter Position (Rückgabewert).
 * Alle Werte links vom Pivot-Element sind kleiner oder gleich dem Wert des Pivot-Elements.
 * Alle Werte rechts vom Pivot-Element sind größer oder gleich dem Wert des Pivot-Elements.
 *
 * @param data Array, das das zu partitionierende Sub-Array enthält.
 * @param left Index des linken Rands des Sub-Arrays.
 * @param right Index des rechten Rands des Sub-Arrays.
 * @return Index des Pivot-Elements nach der Partitionierung.
 */
size_t qsort_partition(unsigned int* data, size_t left, size_t right)
{
	// Linkes Element des Arrays ist Pivot-Element.
	unsigned int pvalue = data[left];

	size_t l = left + 1;
	size_t r = right;

	while (l <= r) {
		// Suche von links ein Element mit einem Wert größer als das Pivot-Element. 
		// Beachte: Es ist erlaubt, dass l bis right+1 zählt, d.h. über das Sub-Array-Ende hinaus zeigt.
		// Wenn l == right, dann überschneiden sich l und r garantiert,
		// und es erfolgt kein (unerlaubter) Tausch zwischen data[right+1] und data[r].
		while (l <= right && data[l] <= pvalue)
			l++;

		// Suche von rechts ein Element mit einem Wert kleiner als das Pivot-Element.
		// Beachte: Es ist erlaubt, dass r bis left zählt. 
		// In diesem Fall ist das Pivot-Element am linken Rand des Sub-Arrays an Position left. 
		// Wenn r == left, dann überschneiden sich l und r garantiert, und es erfolgt kein Tausch zwischen
		// data[left] und data[l]. Das Pivot-Element an Position data[left] wird nach der 
		// while-Schleife mit sich selbst getauscht.
		while (r > left && data[r] >= pvalue)
			r--;

		if (l < r) {
			// Linker und rechter Index haben sich noch nicht überschnitten.
			// Tausche linkes und rechtes Element, so dass Werte kleiner dem Pivot-Element
			// links vom Pivot Element stehen und Werte größer rechts davon.
			unsigned int temp = data[l];
			data[l] = data[r];
			data[r] = temp;
		}
	}

	// Bringe Pivot-Element an die richtige Position durch Tausch von data[left] mit data[r].
	// Das Pivot-Element steht dann an Position r.
	unsigned int temp = data[left];
	data[left] = data[r];
	data[r] = temp;

	return r;
}

/**
 * Führt nicht paralleles Quick-Sort auf einem Sub-Array aus.
 *
 * @param data zu sortierendes Array.
 * @param len Länge des zu sortierenden Arrays.
 */
void qsort(unsigned int* data, size_t left, size_t right)
{
	if (left > right)
		return; // Nicht zu tun.

	size_t pivot = qsort_partition(data, left, right);
	// Achtung: size_t kann keine negativen Werte darstellen.
	// Deswegen müssen wir vor dem rekursiven Aufruf von qsort prüfen, 
	// ob der rechte Index negativ werden würde, wenn das Pivot-Element bereits bei Index 0 steht. 
	// In diesem Fall ist das linke Sub-Array sowieso leer und somit nichts zu tun.
	// Alternativ könnte man ssize_t als Datentyp für den linken Index verwenden, der auch den Wert -1 
	// darstellen kann.
	if (pivot > 0)
		qsort(data, left, pivot - 1);
	qsort(data, pivot + 1, right);
}

/**
 * Quick-Sort.
 *
 * @param data zu sortierendes Array.
 * @param len Länge des zu sortierenden Arrays.
 */
void quicksort(unsigned int* data, size_t len)
{
	if (len == 0)
		return; // Nichts zu tun.

	qsort(data, 0, len - 1);
}

/**
 * Überprüft, ob ein Array sortiert ist.
 *
 * @param data zu überprüfendes Array
 * @param len Länge des zu überprüfenden Arrays.
 * @return true, wenn das Array sortiert ist; false sonst.
 */
bool is_sorted(unsigned int* data, size_t len)
{
	if (len < 2)
		return true;

	for (size_t i = 0; i < len - 2; i++) {
		if (data[i] > data[i + 1])
			return false;
	}

	return true;
}

int main(int argc, char* argv[])
{
	// Erzeuge array_cnt nicht sortierte Arrays mit jeweils array_len Elementen.
	unsigned int* arrays[array_cnt];
	for (size_t i = 0; i < array_cnt; i++) {
		create_random_array(&arrays[i], array_len);
		if (arrays[i] == NULL) {
			fprintf(stderr, "Could not create array to be sorted (%d) \n", GetLastError());
			return 1;
		}
	}

	// Startzeit ermitteln
	DWORD tstart = GetTickCount64();

	// Main-Thread ist Producer und erzeugt für jedes Array einen Sortierauftrag.
	// Consumer verarbeiten Aufträge und sortieren die Arrays nebenläufig.
	for (size_t i = 0; i < array_cnt; i++) {
		// Sortierauftrag anlegen.
		quicksort(arrays[i], array_len);

		// Sanity check: Array tatsächlich sortiert?
		// Dieser Check sollte für die Zeitmessung auskommentiert werden oder der Test
		// im Release-Modus (nicht Debug-Modus) durchgeführt werden, in dem Assertions deaktiviert sind.
		assert(is_sorted(arrays[i], array_len));
	}

	// Endzeit und Laufzeit ermitteln und ausgeben.
	DWORD tend = GetTickCount64();
	DWORD duration = tend - tstart;
	printf("Laufzeit: %d ms\n", duration);

	// Prozess (und damit alle Threads) "hart" beenden.
	return 0;
}
