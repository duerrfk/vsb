/**
 * Copyright 2019 Frank Duerr
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <Ws2tcpip.h>
#include <Windows.h>
#include <stdio.h>

#define BUFFER_SIZE 1000

/*
  TODO: Tragen Sie hier den Hostnamen und Portnummer des Servers ein.
*/
const char* hostname = "localhost";
const char* service = "4242";

/*
  TODO: Definieren Sie die zu sendenden Daten (ein beliebiger String)
*/
const char* str_data = "1234ABCD";
// Datenlänge inklusive terminierender Null-Character.
const size_t str_data_len = strlen(str_data) + 1; 

int main(int argc, char *argv[])
{
	// Winsock DLL initialisieren: Version 2.2 anfragen
	WSADATA wsaData;
	int error = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (error != 0) {
		fprintf(stderr, "WSAStartup() fehlgeschlagen (%d)\n", error);
		return 1;
	}

	// Server-Adressen mit Hilfe von getaddrinfo() ermitteln.
	struct addrinfo hints;
	struct addrinfo *res;
	struct addrinfo *addr;

	/*
	  TODO: Setzen Sie die erforderlichen Anforderungen an die angeforderten
	  Server Adressen in der Datenstruktur hints.
	*/
	memset(&hints, 0, sizeof(hints));
	// Stream-Socket anfragen.
	hints.ai_socktype = /* TODO */;
	// Alle Adressfamilien zulassen (IPv4, IPv6, ...)
	hints.ai_family = /* TODO */;
	// Alle geeigneten Transportprotokolle zulassen (IPPROTO_TCP, ...)
	hints.ai_protocol = /* TODO */;
	// Keine speziellen Flags notwendig.
	hints.ai_flags = 0;
	
	/*
	  TODO: Ermitteln Sie alle Server Adressen mit Hilfe von getaddrinfo().
	*/
	error = getaddrinfo(/* TODO */);
	if (error != 0) {
		fprintf(stderr, "getaddrinfo() fehlgeschlagen (%d)\n", WSAGetLastError());
		WSACleanup();
		return -1;
	}

	/*
	  TODO: Gehen Sie alle ermittelten Adressen durch und versuchen Sie,
	  eine Verbindung zum Server aufzubauen. Verlassen Sie die Schleife, 
	  sobald eine Verbindung aufgebaut werden konnte.
	*/
	int sock = INVALID_SOCKET;
	for (addr = res; addr != NULL; addr = addr->ai_next) {
		/*
		  TODO: Client-Socket erzeugen
		*/ 
		sock = /* TODO */;
		if (sock == INVALID_SOCKET)
			continue;

		/*
		  TODO: Client-Socket mit Server-Socket verbinden
		*/ 
		error = /* TODO */;
		if (error != 0) {
			closesocket(sock);
			sock = INVALID_SOCKET;
			continue;
		}

		// Socket erfolgreich erzeugt und verbunden.
		break;
	}

	freeaddrinfo(res);

	if (sock == INVALID_SOCKET) {
		fprintf(stderr, "Konnte Client nicht mit Server verbinden. \n");
		WSACleanup();
		return 1;
	}

	// Client-Socket ist nun mit Server-Socket verbunden.

	/* 
	  TODO: Daten (str_data) an Server senden.
	*/
	
	/*
	  TODO: Daten vom Server empfangen (null-terminierter String) und in buffer
	  speichern.
	*/
	char buffer[BUFFER_SIZE];
	 
	// Daten (null-terminierter String) auf Bildschirm ausgeben.
	printf("%s", buffer);
	
	// Socket schließen.
	closesocket(sock);

	// Winsock DLL freigeben.
	WSACleanup();
}

