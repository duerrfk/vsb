/**
 * Copyright 2019 Frank Duerr
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Notwendig für rand_s()
#define _CRT_RAND_S

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

const unsigned int seat_cnt = 1000000;
const unsigned int request_cnt = 2000000;
const long seat_free = 0;

// Sitzplatzreservierungen: Sitz frei (seat_free) oder reserviert (Ticketnummer != seat_free).
unsigned int seat_reservations[seat_cnt];

// Index des nächsten freier Platzes, der vergeben wird.
// Sitze werden in aufsteigender (streng monoton steigender) Reihenfolge vergeben: 0, 1, 2, ...
size_t next_free_seat = 0;

/**
 * Generiert eine (mit hoher Wahrscheinlichkeit) eindeutige Eintrittkartennummer.
 * Die generierte Nummer ist garantiert ungleich seat_free.
 *
 * @return Eintrittskartennummer.
 */
unsigned int gen_unique_ticket_number()
{
	unsigned int nr;

	do {
		// rand_s() is thread-safe
		if (rand_s(&nr) != 0)
			exit(1);
	} while (nr == seat_free);

	return nr;
}

/**
 * Alle Plätze als frei markieren.
 */
void init_seat_reservations()
{
	for (size_t i = 0; i < seat_cnt; i++)
		seat_reservations[i] = seat_free;
}

/**
 * Reserviert nächsten freien Platz.
 * Diese Funktion ist *nicht* thread-safe und dient nur zur Demonstration der auftretenden Race-Condition.
 *
 * @param ticket_nummer Eintrittskartennummer, die in der Reservierung eingetragen wird, falls ein Platz reserviert werden kann.
 * @param reserved_seat Liefert bei erfolgreicher Reservierung die Nummer des reservierten Platzes zurück.
 *
 * @return true, bei erfolgreicher Reservierung; false sonst.
 */
bool reserve_seat(unsigned int ticket_nr, size_t* reserved_seat)
{
	if (next_free_seat >= seat_cnt)
		return false;

	*reserved_seat = next_free_seat++;
	seat_reservations[*reserved_seat] = ticket_nr;

	return true;
}

int main(int argc, char* argv[])
{
	size_t reserved_seat;
	size_t accepted_reservations_cnt = 0;
	size_t rejected_reservations_cnt = 0;

	init_seat_reservations();

	for (size_t i = 0; i < request_cnt; i++) {
		unsigned int ticket_nr = gen_unique_ticket_number();
		if (reserve_seat(ticket_nr, &reserved_seat))
			accepted_reservations_cnt++;
		else
			rejected_reservations_cnt++;
	}

	printf("Number of accepted reservations: %d \n", accepted_reservations_cnt);

	return 0;
}
