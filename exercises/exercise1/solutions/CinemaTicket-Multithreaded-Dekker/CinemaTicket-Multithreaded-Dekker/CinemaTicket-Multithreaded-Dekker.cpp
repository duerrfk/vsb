/**
 * Copyright 2019 Frank Duerr
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Notwendig für rand_s()
#define _CRT_RAND_S

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <assert.h>
// Da Visual Studio den stdatomic.h (C11-Standard) nicht unterstützt, verwenden wir die gleichnamigen C++ Typen und Funktionen aus atomic.h
#include <atomic>

// Da Visual Studio den C11-Standard (stdatomic.h) nicht unterstützt, verwenden wir die gleichnamigen C++ Typen und Funktionen.
// Hierzu ist die folgenden Zeile notwendig, um per Default den Standardnamensraum zu verwenden.
// Der C- und C++-Code ist ansonsten identisch!
using namespace std;

const unsigned int seat_cnt = 1000000;
const unsigned int request_cnt = 1000000;
const long seat_free = 0;

// Sitzplatzreservierungen: Sitz frei (seat_free) oder reserviert (Ticketnummer != seat_free).
unsigned int seat_reservations[seat_cnt];

// Index des nächsten freier Platzes, der vergeben wird.
// Sitze werden in aufsteigender (streng monoton steigender) Reihenfolge vergeben: 0, 1, 2, ...
size_t next_free_seat = 0;

// Synchronisationsvariablen
volatile atomic_int turn = 0;
volatile atomic_bool want0 = false;
volatile atomic_bool want1 = false;

/**
 * Generiert eine (mit hoher Wahrscheinlichkeit) eindeutige Eintrittkartennummer.
 * Die generierte Nummer ist garantiert ungleich seat_free.
 *
 * @return Eintrittskartennummer.
 */
unsigned int gen_unique_ticket_number()
{
	unsigned int nr;

	do {
		// rand_s() is thread-safe
		if (rand_s(&nr) != 0)
			exit(1);
	} while (nr == seat_free);

	return nr;
}

/**
 * Alle Plätze als frei markieren.
 */
void init_seat_reservations()
{
	for (size_t i = 0; i < seat_cnt; i++)
		seat_reservations[i] = seat_free;
}

/**
 * Reserviert nächsten freien Platz.
 * Diese Funktion ist nicht thread-safe und dient nur zur Demonstration der auftretenden Race-Condition!
 *
 * @param ticket_nummer Eintrittskartennummer, die in der Reservierung eingetragen wird, falls ein Platz reserviert werden kann.
 * @param reserved_seat Liefert bei erfolgreicher Reservierung die Nummer des reservierten Platzes zurück.
 * @param threadno Thread-Nummer des aufrufenden Threads (0 oder 1).
 *
 * @return true, bei erfolgreicher Reservierung; false sonst.
 */
bool reserve_seat(unsigned int ticket_nr, size_t* reserved_seat, unsigned short threadno)
{
	bool result;

	// Präprotokoll
	switch (threadno) {
	case 0:
		// Signalisiere: Dieser Thread will in KA eintreten.
		atomic_store(&want0, true);
		// Falls der andere Thread auch eintreten will, muss die (faire) Entscheidung über turn fallen.
		while (atomic_load(&want1)) {
			// Falls der andere Thread an der Reihe ist, stelle Eintrittwunsch dieses Threads zurück (want0 = false).
			// -> Der andere Thread kann in KA eintreten.
			// -> Sobald der andere Thread den KA verlassen hat, ist dieser Thread an der Reihe (turn == 0).
			if (atomic_load(&turn) != 0) {
				atomic_store(&want0, false);
				while (atomic_load(&turn) != 0);
				atomic_store(&want0, true);
			}
		}
		break;
	case 1:
		// Signalisiere: Dieser Thread will in KA eintreten.
		atomic_store(&want1, true);
		// Falls der andere Thread auch eintreten will, muss die (faire) Entscheidung über turn fallen.
		while (atomic_load(&want0)) {
			// Falls der andere Thread an der Reihe ist, stelle Eintrittwunsch dieses Threads zurück (want1 = false).
            // -> Der andere Thread kann in KA eintreten.
            // -> Sobald der andere Thread den KA verlassen hat, ist dieser Thread an der Reihe (turn == 1).
			if (atomic_load(&turn) != 1) {
				atomic_store(&want1, false);
				while (atomic_load(&turn) != 1);
				atomic_store(&want1, true);
			}
		}
		break;
	default:
		assert(false);
	}

	// Kritischer Abschnitt: kein anderer Thread führt gleichzeitig hier aus.

	if (next_free_seat >= seat_cnt) {
		result = false;
	} else {
		*reserved_seat = next_free_seat++;
		seat_reservations[*reserved_seat] = ticket_nr;
		result = true;
	}

	// Post-Protokoll
	switch (threadno) {
	case 0:
		// Anderer Thread nun an der Reihe, wenn er in den KA eintreten will.
		atomic_store(&turn, 1);
		atomic_store(&want0, false);
		break;
	case 1:
		// Anderer Thread nun an der Reihe, wenn er in den KA eintreten will.
		atomic_store(&turn, 0);
		atomic_store(&want1, false);
		break;
	default:
		assert(false);
	}

	return result;
}

/**
 * Reservierungs-Thread: Thread 0 führen diese Funktion aus.
 *
 * @param lpParam Zeiger auf Zahl (Typ size_t), die anzeigt, wie viele Sitze der Thread anfragen soll.
 *
 * @return Anzahl der erfolgreich reservierten Plätze.
 */
DWORD WINAPI thread_run_t0(LPVOID lpParam)
{
	size_t* request_cnt;
	size_t reserved_seat;
	size_t accepted_reservations_cnt = 0;
	size_t rejected_reservations_cnt = 0;

	request_cnt = (size_t*)lpParam;

	for (size_t i = 0; i < *request_cnt; i++) {
		unsigned int ticket_nr = gen_unique_ticket_number();
		if (reserve_seat(ticket_nr, &reserved_seat, 0))
			accepted_reservations_cnt++;
		else
			rejected_reservations_cnt++;
	}

	return accepted_reservations_cnt;
}

/**
 * Reservierungs-Thread: Thread 1 führt diese Funktion aus.
 *
 * @param lpParam Zeiger auf Zahl (Typ size_t), die anzeigt, wie viele Sitze der Thread anfragen soll.
 *
 * @return Anzahl der erfolgreich reservierten Plätze.
 */
DWORD WINAPI thread_run_t1(LPVOID lpParam)
{
	size_t* request_cnt;
	size_t reserved_seat;
	size_t accepted_reservations_cnt = 0;
	size_t rejected_reservations_cnt = 0;

	request_cnt = (size_t*)lpParam;

	for (size_t i = 0; i < *request_cnt; i++) {
		unsigned int ticket_nr = gen_unique_ticket_number();
		if (reserve_seat(ticket_nr, &reserved_seat, 1))
			accepted_reservations_cnt++;
		else
			rejected_reservations_cnt++;
	}

	return accepted_reservations_cnt;
}

int main(int argc, char* argv[])
{
	// Initialisiere Sitzplatzreservierungen (alle frei).
	init_seat_reservations();

	// Erzeuge zwei Threads (Thread 0 und Thread 1) für nebenläufige Sitzplatzreservierungen.
	// Jeder Thread führt REQUEST_CNT Anfragen aus.
	size_t arg_request_cnt = request_cnt;
	HANDLE hThread0;
    hThread0 = CreateThread(
		NULL,                   // default security attributes
		0,                      // default stack size  
		thread_run_t0,          // thread function
		&arg_request_cnt,       // arguments to thread function 
		0,                      // use default creation flags 
		NULL);                  // don't store thread identifier 
	if (hThread0 == NULL) {
		printf("Konnte Thread nicht erzeugen (%d). \n", GetLastError());
		return 1;
	}

	HANDLE hThread1;
	hThread1 = CreateThread(
		NULL,                   // default security attributes
		0,                      // default stack size  
		thread_run_t1,          // thread function
		&arg_request_cnt,       // arguments to thread function 
		0,                      // use default creation flags 
		NULL);                  // don't store thread identifier 
	if (hThread1 == NULL) {
		printf("Konnte Thread nicht erzeugen (%d). \n", GetLastError());
		return 1;
	}

	// Auf beide Threads warten und Anzahl erfolgreich reservierter Sitze aller Threads aufsummieren.
	size_t total_accepted_reservation_cnt = 0;
	if (WaitForSingleObject(hThread0, INFINITE) == WAIT_FAILED) {
		fprintf(stderr, "Konnte nicht auf Thread warten. \n");
		return 2;
	}
	DWORD accepted_reservation_cnt;
	if (!GetExitCodeThread(hThread0, &accepted_reservation_cnt)) {
		fprintf(stderr, "Konnte Thread-Exit-Code nicht ermitteln. \n");
		return 3;
	}
	total_accepted_reservation_cnt += accepted_reservation_cnt;

	if (WaitForSingleObject(hThread1, INFINITE) == WAIT_FAILED) {
		fprintf(stderr, "Konnte nicht auf Thread warten. \n");
		return 2;
	}
	if (!GetExitCodeThread(hThread1, &accepted_reservation_cnt)) {
		fprintf(stderr, "Konnte Thread-Exit-Code nicht ermitteln. \n");
		return 3;
	}
	total_accepted_reservation_cnt += accepted_reservation_cnt;
	
	printf("Insgesamt akzeptierte Anzahl an Reservierungen: %d \n", total_accepted_reservation_cnt);

	return 0;
}
