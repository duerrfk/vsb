/**
 * Copyright 2019 Frank Duerr
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Notwendig für rand_s()
#define _CRT_RAND_S

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>
// Da Visual Studio den stdatomic.h (C11-Standard) nicht unterstützt, verwenden wir die gleichnamigen C++ Typen und Funktionen aus atomic.h
#include <atomic>

// Da Visual Studio den C11-Standard (stdatomic.h) nicht unterstützt, verwenden wir die gleichnamigen C++ Typen und Funktionen.
// Hierzu ist die folgenden Zeile notwendig, um per Default den Standardnamensraum zu verwenden.
// Der C- und C++-Code ist ansonsten identisch!
using namespace std;

const unsigned int seat_cnt = 1000000;
const unsigned int request_cnt = 1000000;
const unsigned int thread_cnt = 10;
const long seat_free = 0;
 
// Sitzplatzreservierungen: Sitz frei (seat_free) oder reserviert (Ticketnummer != seat_free).
unsigned int seat_reservations[seat_cnt];

// Index des nächsten freier Platzes, der vergeben wird.
// Sitze werden in aufsteigender (streng monoton steigender) Reihenfolge vergeben: 0, 1, 2, ...
size_t next_free_seat = 0;

// Synchronisationsvariablen.
volatile atomic_flag mutex = ATOMIC_FLAG_INIT;

/**
 * Generiert eine (mit hoher Wahrscheinlichkeit) eindeutige Eintrittkartennummer.
 * Die generierte Nummer ist garantiert ungleich seat_free.
 *
 * @return Eintrittskartennummer.
 */
unsigned int gen_unique_ticket_number()
{
	unsigned int nr;

	do {
		// rand_s() is thread-safe
		if (rand_s(&nr) != 0)
			exit(1);
	} while (nr == seat_free);

	return nr;
}

/**
 * Mutex durch den aufrufenden Thread sperren.
 * Sobald das MUTEX durch den aufrufenden Thread gesperrt wurde, kehrt diese Funktion zurück.
 * So lange das MUTEX nicht gesperrt werden konnte, wird der aufrufende Thread aktiv auf die Sperre warten.
 *
 * @param mutex Zeiger auf MUTEX-Variable.
 */
void lock_acquire(volatile atomic_flag* mutex)
{
	// Busy waiting ("spinning") to enter critical section.
	while (atomic_flag_test_and_set(mutex));

	// Note: acquire semantic (implied by sequential consistency)
	// ensures that every memory access below (in critical section)
	// stays below (in critical section).
}

/**
 * Entsperrt ein zuvor gesperrtes MUTEX.
 *
 * @param mutex Zeiger auf MUTEX-Variable.
 */
void lock_release(volatile atomic_flag* mutex)
{
	// Note: release semantic (implied by sequential consistency)
	// ensures that every memory access above (in critical section)
	// stays above (in critical section).

	atomic_flag_clear(mutex);
}

/**
 * Alle Plätze als frei markieren.
 */
void init_seat_reservations()
{
	for (size_t i = 0; i < seat_cnt; i++)
		seat_reservations[i] = seat_free;
}

/**
 * Reserviert nächsten freien Platz.
 * Diese Funktion ist nicht thread-safe und dient nur zur Demonstration der auftretenden Race-Condition!
 *
 * @param ticket_nummer Eintrittskartennummer, die in der Reservierung eingetragen wird, falls ein Platz reserviert werden kann.
 * @param reserved_seat Liefert bei erfolgreicher Reservierung die Nummer des reservierten Platzes zurück.
 *
 * @return true, bei erfolgreicher Reservierung; false sonst.
 */
bool reserve_seat(unsigned int ticket_number, size_t* reserved_seat)
{
	bool res;

	// Kritischen Abschnitt mit MUTEX-Variable sperren.
	lock_acquire(&mutex);

	if (next_free_seat >= seat_cnt) {
		res = false;
	} else {
		*reserved_seat = next_free_seat++;
		seat_reservations[*reserved_seat] = ticket_number;
		res = true;
	}

	// Kritischen Abschnitt (MUTEX-Variable) entsperren.
	lock_release(&mutex);

	return res;
}

/**
 * Reservierungs-Threads führen diese Funktion aus.
 *
 * @param lpParam Zeiger auf Zahl (Typ size_t), die anzeigt, wie viele Sitze der Thread anfragen soll.
 *
 * @return Anzahl der erfolgreich reservierten Plätze.
 */
DWORD WINAPI thread_run(LPVOID lpParam)
{
	size_t* request_cnt;
	size_t reserved_seat;
	size_t accepted_reservations_cnt = 0;
	size_t rejected_reservations_cnt = 0;

	request_cnt = (size_t*)lpParam;

	for (size_t i = 0; i < *request_cnt; i++) {
		unsigned int ticket_nr = gen_unique_ticket_number();
		if (reserve_seat(ticket_nr, &reserved_seat))
			accepted_reservations_cnt++;
		else
			rejected_reservations_cnt++;
	}

	return accepted_reservations_cnt;
}

int main(int argc, char* argv[])
{
	// Initialisiere Sitzplatzreservierungen (alle frei).
	init_seat_reservations();

	// Erzeuge THREAD_CNT Threads für nebenläufige Sitzplatzreservierungen.
	// Jeder Thread führt REQUEST_CNT Anfragen aus.
	size_t arg_request_cnt = request_cnt;
	HANDLE hThreads[thread_cnt];
	for (size_t i = 0; i < thread_cnt; i++) {
		hThreads[i] = CreateThread(
			NULL,                   // default security attributes
			0,                      // default stack size  
			thread_run,             // thread function
			&arg_request_cnt,       // arguments to thread function 
			0,                      // use default creation flags 
			NULL);                  // don't store thread identifier 
		if (hThreads[i] == NULL) {
			printf("Konnte Thread nicht erzeugen (%d). \n", GetLastError());
			return 1;
		}
	}

	// Auf alle Threads warten und Anzahl erfolgreich reservierter Sitze aller Threads aufsummieren.
	size_t total_accepted_reservation_cnt = 0;
	for (size_t i = 0; i < thread_cnt; i++) {
		if (WaitForSingleObject(hThreads[i], INFINITE) == WAIT_FAILED) {
			fprintf(stderr, "Konnte nicht auf Thread warten. \n");
			return 2;
		}

		DWORD accepted_reservation_cnt;
		if (!GetExitCodeThread(hThreads[i], &accepted_reservation_cnt)) {
			fprintf(stderr, "Konnte Thread-Exit-Code nicht ermitteln. \n");
			return 3;
		}

		total_accepted_reservation_cnt += accepted_reservation_cnt;
	}

	printf("Insgesamt akzeptierte Anzahl an Reservierungen: %d \n", total_accepted_reservation_cnt);

	return 0;
}
