// Notwendig für rand_s()
#define _CRT_RAND_S

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>
// Da Visual Studio den stdatomic.h (C11-Standard) nicht unterstützt, verwenden wir die gleichnamigen C++ Typen und Funktionen aus atomic.h
#include <atomic>

// Da Visual Studio den C11-Standard (stdatomic.h) nicht unterstützt, verwenden wir die gleichnamigen C++ Typen und Funktionen.
// Hierzu ist die folgenden Zeile notwendig, um per Default den Standardnamensraum zu verwenden.
// Der C- und C++-Code ist ansonsten identisch!
using namespace std;

const size_t seat_cnt = 1000000;
const unsigned int request_cnt = 1000000;
const unsigned int thread_cnt = 10;
const long seat_free = 0;

// Sitzplatzreservierungen: Sitz frei (seat_free) oder reserviert (Ticketnummer != seat_free).
unsigned int seat_reservations[seat_cnt];

// Index des nächsten freier Platzes, der vergeben wird.
// Sitze werden in aufsteigender (streng monoton steigender) Reihenfolge vergeben: 0, 1, 2, ...
size_t next_free_seat = 0;

/**
 * Generiert eine (mit hoher Wahrscheinlichkeit) eindeutige Eintrittkartennummer.
 * Die generierte Nummer ist garantiert ungleich seat_free.
 *
 * @return Eintrittskartennummer.
 */
unsigned int gen_unique_ticket_number()
{
	unsigned int nr;

	do {
		// rand_s() is thread-safe
		if (rand_s(&nr) != 0)
			exit(1);
	} while (nr == seat_free);

	return nr;
}

/**
 * Alle Plätze als frei markieren.
 */
void init_seat_reservations()
{
	// TODO
}

/**
 * Reserviert nächsten freien Platz.
 *
 * @param ticket_nummer Eintrittskartennummer, die in der Reservierung eingetragen wird, falls ein Platz reserviert werden kann.
 * @param reserved_seat Liefert bei erfolgreicher Reservierung die Nummer des reservierten Platzes zurück.
 *
 * @return true, bei erfolgreicher Reservierung; false sonst.
 */
bool reserve_seat(unsigned int ticket_number, size_t* reserved_seat)
{
	// TODO
}

int main(int argc, char* argv[])
{
	// Initialisiere Sitzplatzreservierungen (alle frei).
	init_seat_reservations();

	// Führe request_cnt Sitzplatzanfragen durch.
	// Summiere die Anzahl erfolgreicher Reservierungen auf.
	// TODO

	// Gebe die Anzahl erfolgreicher Reservierungen aus.
	// TODO

	return 0;
}